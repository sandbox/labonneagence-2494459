<?php

/**
 * Affichage des informations sur la plateforme
 */
function lba_insight_plateform_overview() {
  
  if(isset($_GET["site"])) {
  
    module_load_include("inc", "lba_insight_master","sites");
  
    $sid = $_GET["site"];
    $site = db_select("insight_site","s")
    ->condition("s.sid",$sid)
    ->fields("s",array("sid","sitename","url","token","score"))
    ->execute()->fetchAssoc();
  
    drupal_set_title(t("LBA Insight - Plateform for :sitename",array(":sitename"=>$site["sitename"])),PASS_THROUGH);
  
    $data = array();
    
    //ID du site consulté
    $data["site"] = $sid;
    
    $infos = lba_insight_master_get_info_site($sid);
  
    //scores
    $data["score"] = $site["score"]?$site["score"]:0;
    
    //Infos PHP / MYSQL / APACHE
    $data["data"] = $infos["plateform"];
    
  } else {
  
    drupal_set_title("LBA Insight - Plateform");
    $data = array();
  
    //scores
    $data["score"] = lba_insight_get_score();
    
    //Infos PHP / MYSQL / APACHE
    $data["data"] = lba_insight_plateform_data();
    
  }
  return theme("plateform_overview",$data);
}


/**
 * Gather platform specific information.
 * @return An associative array keyed by a platform information type.
 */
function lba_insight_plateform_data() {
  $server = $_SERVER;
  // Database detection depends on the structure starting with the database
  $db_class = 'DatabaseTasks_' . Database::getConnection()->driver();
  $db_tasks = new $db_class();
  // Webserver detection is based on name being before the slash, and
  // version being after the slash.
  preg_match('!^([^/]+)(/.+)?$!', $server['SERVER_SOFTWARE'], $webserver);

  if (isset($webserver[1]) && stristr($webserver[1], 'Apache') && function_exists('apache_get_version')) {
    $webserver[2] = apache_get_version();
    $apache_modules = apache_get_modules();
  }
  else {
    $apache_modules = '';
  }

  // Get some basic PHP vars
  $php_quantum = array(
      'memory_limit'            => ini_get('memory_limit'),
      'register_globals'        => ini_get('register_globals'),
      'post_max_size'           => ini_get('post_max_size'),
      'max_execution_time'      => ini_get('max_execution_time'),
      'upload_max_filesize'     => ini_get('upload_max_filesize'),
      'error_log'               => ini_get('error_log'),
      'error_reporting'         => ini_get('error_reporting'),
      'display_errors'          => ini_get('display_errors'),
      'log_errors'              => ini_get('log_errors'),
      'session.cookie_domain'   => ini_get('session.cookie_domain'),
      'session.cookie_lifetime' => ini_get('session.cookie_lifetime'),
      'newrelic.appname'        => ini_get('newrelic.appname'),
      'SERVER'                  => $server,
      'sapi'                    => php_sapi_name(),
  );

  $platform = array(
      'php'               => PHP_VERSION,
      'webserver_type'    => isset($webserver[1]) ? $webserver[1] : '',
      'webserver_version' => isset($webserver[2]) ? $webserver[2] : '',
      'apache_modules'    => $apache_modules,
      'php_extensions'    => get_loaded_extensions(),
      'php_quantum'       => $php_quantum,
      'database_type'     => $db_tasks->name(),
      'database_version'  => Database::getConnection()->version(),
      'system_type'       => php_uname('s'),
      // php_uname() only accepts one character, so we need to concatenate ourselves.
      'system_version'    => php_uname('r') . ' ' . php_uname('v') . ' ' . php_uname('m') . ' ' . php_uname('n'),
      'mysql'             => (Database::getConnection()->driver() == 'mysql') ? lba_insight_plateform_mysql_data() : array(),
  );

  // Never send NULL (or FALSE?) - that causes hmac errors.
  foreach ($platform as $key => $value) {
    if (empty($platform[$key])) {
      $platform[$key] = '';
    }
  }
  return $platform;
}

function lba_insight_plateform_mysql_data() {
  $connection = Database::getConnection('default');
  // Backup and restore PDO::ATTR_CASE. Prior to Drupal 7.14 this is
  // PDO::CASE_LOWER. Afterwards it is PDO::CASE_NATURAL.
  $orig = $connection->getAttribute(PDO::ATTR_CASE);
  $connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
  $result = $connection->query("SHOW GLOBAL STATUS", array(), array());
  // Restore the attribute.
  $connection->setAttribute(PDO::ATTR_CASE, $orig);

  $ret = array();
  if (empty($result)) {
    return $ret;
  }
  foreach ($result as $record) {
    if (!isset($record->variable_name)) {
      continue;
    }
    switch ($record->variable_name) {
      case 'Table_locks_waited':
        $ret['Table_locks_waited'] = $record->value;
        break;
      case 'Slow_queries':
        $ret['Slow_queries'] = $record->value;
        break;
      case 'Qcache_hits':
        $ret['Qcache_hits'] = $record->value;
        break;
      case 'Qcache_inserts':
        $ret['Qcache_inserts'] = $record->value;
        break;
      case 'Qcache_queries_in_cache':
        $ret['Qcache_queries_in_cache'] = $record->value;
        break;
      case 'Qcache_lowmem_prunes':
        $ret['Qcache_lowmem_prunes'] = $record->value;
        break;
      case 'Open_tables':
        $ret['Open_tables'] = $record->value;
        break;
      case 'Opened_tables':
        $ret['Opened_tables'] = $record->value;
        break;
      case 'Select_scan':
        $ret['Select_scan'] = $record->value;
        break;
      case 'Select_full_join':
        $ret['Select_full_join'] = $record->value;
        break;
      case 'Select_range_check':
        $ret['Select_range_check'] = $record->value;
        break;
      case 'Created_tmp_disk_tables':
        $ret['Created_tmp_disk_tables'] = $record->value;
        break;
      case 'Created_tmp_tables':
        $ret['Created_tmp_tables'] = $record->value;
        break;
      case 'Handler_read_rnd_next':
        $ret['Handler_read_rnd_next'] = $record->value;
        break;
      case 'Sort_merge_passes':
        $ret['Sort_merge_passes'] = $record->value;
        break;
      case 'Qcache_not_cached':
        $ret['Qcache_not_cached'] = $record->value;
        break;

    }
  }
  return $ret;
}