<?php

/**
 * Affichage des informations sur le référencement
 */
function lba_insight_seo_overview() {
  
  
  if(isset($_GET["site"])) {
  
    module_load_include("inc", "lba_insight_master","sites");
  
    $sid = $_GET["site"];
    $site = db_select("insight_site","s")
    ->condition("s.sid",$sid)
    ->fields("s",array("sid","sitename","url","token","score"))
    ->execute()->fetchAssoc();
  
    drupal_set_title(t("LBA Insight - SEO for :sitename",array(":sitename"=>$site["sitename"])),PASS_THROUGH);
    
    $data = array();
    
    $infos = lba_insight_master_get_info_site($sid);
    
    //scores
    $data["score"] =  $site["score"]?$site["score"]:0;
    $data["score_seo"] = $infos["seo"]["score"]?$infos["seo"]["score"]:0;
    
    //Récupération des tests à effectuer
    $data["tests"] = $infos["seo"]["tests"];
    
    //Exécution des tests
    $ret = $infos["seo"]["state"];
   
  } else {
  
    drupal_set_title("LBA Insight - SEO");
    $data = array();
    
    //scores
    $data["score"] = lba_insight_get_score();
    $data["score_seo"] = lba_insight_get_score("seo");
    
    //Récupération des tests à effectuer
    $data["tests"] = lba_insight_seo_tests();
    
    //Exécution des tests
    $ret = lba_insight_make_tests("seo",$data["tests"]);
  
  }
  
  return theme("seo_overview",$data);
}


/**
 * Liste des tests effectués par le module
 */
function lba_insight_seo_tests() {
  global $base_url;
  return array(
      "ga" => array(
          "description" => t("Activation de Google Analytics"),
          "help"=> t("Installer et activer le module !link",array("!link"=>l(t("Google Analytics"),"https://www.drupal.org/project/google_analytics",array("attributes"=>array("target"=>"_blank"))))),
          "importance" => ERROR,
      ),
      "xmlsitemap" => array(
          "description" => t("Activation du module XML sitemap"),
          "help"=> t("Installer et activer le module !link",array("!link"=>l(t("XML sitemap"),"https://www.drupal.org/project/xmlsitemap",array("attributes"=>array("target"=>"_blank"))))),
          "importance" => ERROR,
      ),
      "xmlsitemap_access" => array(
          "description" => t("Accès au XML sitemap"),
          "help"=> t("Vérifier l'accès au !file",array("!file"=>l(t("sitemap.xml"),$base_url."/sitemap.xml",array("attributes"=>array("target"=>"_blank"))))),
          "importance" => ERROR,
      ),
      "robots" => array(
          "description" => t("Le fichier robots.txt est présent et accessible par les robots"),
          "help"=> t("Vérifier la présence du fichier !file et supprimer la directive Disallow: / ",array("!file"=>l(t("robots.txt"),$base_url."/robots.txt",array("attributes"=>array("target"=>"_blank"))))),
          "importance" => ERROR,
      ),
  );
}

/**
 * Test si le module google_analytics est activé
 * @return boolean true si actif false sinon
 */
function lba_insight_seo_ga() {
  return module_exists('googleanalytics');
}

/**
 * Test si le module xmlsitemap est activé
 * @return boolean true si actif false sinon
 */
function lba_insight_seo_xmlsitemap() {
  return module_exists('xmlsitemap') || lba_insight_seo_xmlsitemap_access();
}

/**
 * Test si le xmlsitemap est accessible
 * @return boolean true si accessible false sinon
 */
function lba_insight_seo_xmlsitemap_access() {
  global $base_url;
  $path = "sitemap.xml";
  $url  = rtrim($base_url, DIRECTORY_SEPARATOR) . base_path() . $path;
  $response = drupal_http_request($url);
  return (isset($response->code) && $response->code != 404);
}

/**
 * Test si le robots.txt est accessible et directive Disallow: / non présente
 * @return boolean true si accessible false sinon
 */
function lba_insight_seo_robots() {
  global $base_url;

  $agents = array(preg_quote('*'));
  $agents = implode('|', $agents);

  // location of robots.txt file
  $robotstxt = @file($base_url."/robots.txt");

  // if there isn't a robots, then we're allowed in
  if(empty($robotstxt)) return false;

  $rules = array();
  $ruleApplies = false;
  foreach($robotstxt as $line) {
    // skip blank lines
    if(!$line = trim($line)) continue;

    // following rules only apply if User-agent matches $useragent or '*'
    if(preg_match('/^\s*User-agent: (.*)/i', $line, $match)) {
        $ruleApplies = preg_match("/($agents)/i", $match[1]);
    }
    if($ruleApplies && preg_match('/^\s*Disallow:(.*)/i', $line, $regs)) {
      // an empty rule implies full access - no further tests required
      if(!$regs[1]) return true;
      // add rules that apply to array for testing
      $rules[] = preg_quote(trim($regs[1]), '/');
    }
  }
  foreach($rules as $rule) {
    // check if page is disallowed to us
    if(preg_match("/^$rule/", "/")) return false;
  }

  // page is not disallowed
  return true;
}