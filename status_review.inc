<?php

/**
 * Affichage des informations des différentes analyses
 */
function lba_insight_overview() {
  global $base_url;
  
  if(isset($_GET["site"]) && $_GET["site"]) {
    
    module_load_include("inc", "lba_insight_master","sites");
    
    $sid = $_GET["site"];
    $site = db_select("insight_site","s")
    ->condition("s.sid",$sid)
    ->fields("s",array("sid","sitename","url","token","score"))
    ->execute()->fetchAssoc();
    
    drupal_set_title(t("LBA Insight - Overview for :sitename",array(":sitename"=>$site["sitename"])),PASS_THROUGH);
    
    $data = array();
    
    //ID du site consulté
    $data["site"] = $sid;
    
    $infos = lba_insight_master_get_info_site($sid);
    
    //scores
    $data["score"] = $site["score"]?$site["score"]:0;
    $data["score_performance"] = $infos["performance"]["score"]?$infos["performance"]["score"]:0;
    $data["score_security"] = $infos["security"]["score"]?$infos["security"]["score"]:0;
    $data["score_seo"] = $infos["seo"]["score"]?$infos["seo"]["score"]:0;
    
    //info
    $data["base_url"] = $site["url"];
    $data["token"] = $site["token"];
    $data["version"] = lba_insight_master_version_site($sid);
    
  } else {
  
    drupal_set_title(t("LBA Insight - Overview"));
    $data = array();
    
    $data["site"] = NULL;
    
    //scores
    $data["score"] = lba_insight_get_score();
    $data["score_performance"] = lba_insight_get_score("performance");
    $data["score_security"] = lba_insight_get_score("security");
    $data["score_seo"] = lba_insight_get_score("seo");
    
    //info
    $data["base_url"] = $base_url;
    $data["token"] = variable_get("cron_key","");
    $data["version"] = VERSION;
  }
  
  return theme("insight_overview",$data);
}


/**
 * Export JSON des informations du site à LBA Insight
 */
function lba_insight_export_data() {
  
  module_load_include("inc", "lba_insight","performance_review");
  module_load_include("inc", "lba_insight","security_review");
  module_load_include("inc", "lba_insight","plateform_review");
  module_load_include("inc", "lba_insight","seo_review");
  
  //Test du token
  $token = $_GET["token"];
  if($token!=variable_get("cron_key","")) {
    return drupal_access_denied();
  }
  
  $data = array();
  
  //Score global
  $data["score"] = lba_insight_get_score();
  
  //Tests et score
  $data["security"] = array();
  $data["security"]["tests"] = lba_insight_security_tests();
  $ret = lba_insight_make_tests("security",$data["security"]["tests"]);
  $data["security"]["state"] = $ret;
  $data["security"]["score"] = lba_insight_get_score("security");
  
  $data["performance"] = array();
  $data["performance"]["tests"]  = lba_insight_performance_tests();
  $ret = lba_insight_make_tests("performance",$data["performance"]["tests"]);
  $data["performance"]["state"] = $ret;
  $data["performance"]["score"] = lba_insight_get_score("performance");
  
  $data["seo"] = array();
  $data["seo"]["tests"]  = lba_insight_seo_tests();
  $ret = lba_insight_make_tests("seo",$data["seo"]["tests"]);
  $data["seo"]["state"] = $ret;
  $data["seo"]["score"] = lba_insight_get_score("seo");
  
  $data["plateform"] = array();
  $data["plateform"] = lba_insight_plateform_data();
  
  header('content-type:text/html;charset=utf-8');
  echo drupal_json_encode($data);
  exit();
}

/**
 * Vérification si le site répond bien
 */
function lba_insight_isalive() {
  $isalive = true;
  
  //Test du token
  $token = $_GET["token"];
  if($token!=variable_get("cron_key","")) {
    return drupal_access_denied();
  }
  
  //Test connexion DB
  $isalive = $isalive && db_table_exists("variable");
  
  echo drupal_json_encode($isalive?1:0);
  exit();
}

/**
 * Version du site
 */
function lba_insight_version() {
  $version = "";

  //Test du token
  $token = $_GET["token"];
  if($token!=variable_get("cron_key","")) {
    return drupal_access_denied();
  }

  //Test connexion DB
  $version = VERSION;

  echo drupal_json_encode($version);
  exit();
}