<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="jumbotron insight-score-header">
        <h2><?php print t("Insight score"); ?></h2>
        <p><?php print t("The Insight score for your site is representative of your security, performance, and best practices scores."); ?>
        <p class="score <?php print lba_insight_get_global_class($score); ?>"><?php print $score;?><sup>%</sup></p>
      </div>
    </div>
    
    <div class="col-sm-12 summary">
     <h2><?php print t("Analysis summary"); ?></h2>
     
     <h3><?php print l(t("Performance"),"admin/config/insight/overview/performance",array("query"=>$site?array("site"=>$site):NULL)); ?> : </h3>
     <div class="progress">
       <div class="progress-bar progress-bar-<?php print lba_insight_get_score_class($score_performance);?> progress-bar-striped" role="progressbar" aria-valuenow="<?php print $score_performance;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $score_performance;?>%">
          <span><?php print $score_performance;?><sup>%</sup></span>
       </div>
     </div>
      
     <h3><?php print l(t("Security"),"admin/config/insight/overview/security",array("query"=>$site?array("site"=>$site):NULL)); ?> : </h3>
     <div class="progress">
       <div class="progress-bar progress-bar-<?php print lba_insight_get_score_class($score_security);?> progress-bar-striped" role="progressbar" aria-valuenow="<?php print $score_security;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $score_security;?>%">
          <span><?php print $score_security;?><sup>%</sup></span>
       </div>
     </div>
      
     <h3><?php print l(t("SEO"),"admin/config/insight/overview/seo",array("query"=>$site?array("site"=>$site):NULL)); ?> : </h3>
     <div class="progress">
       <div class="progress-bar progress-bar-<?php print lba_insight_get_score_class($score_seo);?> progress-bar-striped" role="progressbar" aria-valuenow="<?php print $score_seo;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $score_seo;?>%">
          <span><?php print $score_seo;?><sup>%</sup></span>
       </div>
     </div>
     
    </div>
    
    <div class="col-sm-12 token">
      <h2><?php print t("Insight keys"); ?></h2>
      <p><?php print t("Use the token and the site URL to plug to the LBA Insight Master site");?></p>
      <dl class="dl-horizontal">
        <dt><?php print t("Version Drupal");?></dt>
        <dd><?php print $version;?></dd>
        <dt><?php print t("Token");?></dt>
        <dd><?php print $token;?></dd>
        <dt><?php print t("URL");?></dt>
        <dd><?php print $base_url;?></dd>
      </dl>
      <p></p>
    </div>
  
  </div>
</div>
