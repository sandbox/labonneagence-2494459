<div class="container">
  <div class="row">
    
    <div class="col-sm-12">
      <div class="jumbotron insight-score-header">
        <h2><?php print t("Insight score"); ?></h2>
        <p><?php print t("The Insight score for your site is representative of your security, performance, and best practices scores."); ?>
        <p class="score <?php print lba_insight_get_global_class($score); ?>"><?php print $score;?><sup>%</sup></p>
      </div>
    </div>
    
    <div class="col-sm-12">
    <pre><?php print_r($data); ?></pre>
    </div>
    
  </div>
</div>
    