<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="jumbotron insight-score-header">
        <h2><?php print t("Insight score"); ?></h2>
        <p><?php print t("The Insight score for your site is representative of your security, performance, and best practices scores."); ?>
        <p class="score <?php print lba_insight_get_global_class($score); ?>"><?php print $score;?><sup>%</sup></p>
      </div>
    </div>
    
    <div class="col-sm-12 summary">
     <div class="progress">
      <div class="progress-bar progress-bar-<?php print lba_insight_get_score_class($score_seo);?> progress-bar-striped" role="progressbar" aria-valuenow="<?php print $score_seo;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $score_seo;?>%">
        <span><?php print $score_seo;?><sup>%</sup></span>
      </div>
      </div>
    </div>
    
    <div class="col-sm-12 detail">
      <table class="table">
        <?php foreach($tests as $test) { 
            $class="active";
            $icon="glyphicon-remove";
            if($test["state"]==OK) { 
              $class="success";
              $icon="glyphicon-ok";
            } elseif($test["state"]==ERROR) { 
              $class="danger";
              $icon="glyphicon-remove";
            } elseif($test["state"]==WARNING) {
              $class="warning";
              $icon="glyphicon-remove";
            } elseif($test["state"]==NOTICE) {
              $class="info";
              $icon="glyphicon-remove";
            }
            ?>
            <tr class="<?php print $class;?>">
              <td><div class="title"><?php print $test["description"];?></div><div class="small help"><?php print $test["help"];?></div></td>
              <td><span class="glyphicon <?php print $icon;?>" aria-hidden="true"></span></td>
            </tr>
        <?php } ?>
      </table>
    </div>
  
  </div>
</div>
