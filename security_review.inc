<?php

/**
 * Affichage des informations sur la version de Drupal, les mises à jour de sécurité ...
 */
function lba_insight_security_overview() {
  
  
  if(isset($_GET["site"])) {
  
    module_load_include("inc", "lba_insight_master","sites");
  
    $sid = $_GET["site"];
    $site = db_select("insight_site","s")
    ->condition("s.sid",$sid)
    ->fields("s",array("sid","sitename","url","token","score"))
    ->execute()->fetchAssoc();
  
    drupal_set_title(t("LBA Insight - Security for :sitename",array(":sitename"=>$site["sitename"])),PASS_THROUGH);
  
    $data = array();
  
    $infos = lba_insight_master_get_info_site($sid);
    
    //scores
    $data["score"] = $site["score"]?$site["score"]:0;
    $data["score_security"] = $infos["security"]["score"]?$infos["security"]["score"]:0;
    
    //Récupération des tests à effectuer
    $data["tests"] = $infos["security"]["tests"];
    
    //Exécution des tests
    $ret = $infos["security"]["state"];
    
  } else {
  
    drupal_set_title("LBA Insight - Security");
    $data = array();
    
    //scores
    $data["score"] = lba_insight_get_score();
    $data["score_security"] = lba_insight_get_score("security");
    
    //Récupération des tests à effectuer
    $data["tests"] = lba_insight_security_tests();
    
    //Exécution des tests
    $ret = lba_insight_make_tests("security",$data["tests"]);
  
  }
  
  return theme("security_overview",$data);
}

/**
 * Liste des tests effectués par le module
 */
function lba_insight_security_tests() {
  
  //Lien de téléchargement pour nouvelle version de Drupal
  $drupal_download_link = "";
  $release = variable_get("lba_insigth_performance_version_available",NULL);
  if($release["version"]) {
    $drupal_download_link .= " ".l(t("Télécharger la version @num",array("@num"=>$release["version"])),$release["download_link"],array("attributes"=>array("target"=>"_blank")));
  }
  
  //Listes des modules à mettre à jour + liens de téléchargement
  $modules_download_link = " <ul>";
  $releases = variable_get("lba_insigth_performance_modules_to_update",array());
  foreach($releases as $release) {
    if($release["version"]) {
      $modules_download_link .= "<li>".$release["old_version"]." ".l(t("Télécharger la version @num",array("@num"=>$release["version"])),$release["download_link"],array("attributes"=>array("target"=>"_blank")))."</li>";
    }
  }
  $modules_download_link .= "</ul>";
  
  return array(
      "core_status" => array(
          "description" => t("Version du core de Drupal @num",array("@num"=>VERSION)),
          "help"=> t("Mettre à jour la version de Drupal @num",array("@num"=>VERSION)).$drupal_download_link,
          "importance" => ERROR,
      ),
      "modules_status" => array(
          "description" => t("Mise à jour des modules"),
          "help"=> t("Effectuer les mises à jour de sécurité des modules").$modules_download_link,
          "importance" => ERROR,
      ),
      "check_files_present" => array(
          "description" => t("Supprimer les fichiers d'installation"),
          "help"=> t("Supprimer les fichiers d'installation à la racine du site :<ul><li>CHANGELOG.txt</li><li>COPYRIGHT.txt</li><li>INSTALL.mysql.txt</li><li>INSTALL.pgsql.txt</li><li>INSTALL.txt</li><li>LICENSE.txt</li><li>MAINTAINERS.txt</li><li>README.txt</li><li>UPGRADE.txt</li><li>PRESSFLOW.txt</li><li>install.php</li></ul>"),
          "importance" => ERROR,
      ),
      "settings_permissions" => array(
          "description" => t("Lecture seule sur le fichier settings.php"),
          "help"=> t("Définir les bons droits d'accès sur le fichiers settings.php"),
          "importance" => ERROR,
      ),
      "anonyme_exist" => array(
          "description" => t("L'utilisateur anonyme existe"),
          "help"=> t("L'utilisateur anonyme UID=0 doit exister"),
          "importance" => ERROR,
      ),
      "admin_exist" => array(
          "description" => t("L'utilisateur super-admin existe"),
          "help"=> t("L'utilisateur super-admin UID=1 doit exister"),
          "importance" => ERROR,
      ),
      "access_update" => array(
          "description" => t("Accéder à update.php"),
          "help"=> t("L'accès à la mise de Drupal doit êre protégé dans le fichier settings.php : \$update_free_access = FALSE;"),
          "importance" => ERROR,
      ),
      "maintenance" => array(
          "description" => t("Le site est en ligne"),
          "help"=> t("Mettre le site en ligne [URL]/admin/config/development/maintenance"),
          "importance" => WARNING,
      ),
      "super_name" => array(
          "description" => t("Nom du super-administrateur"),
          "help"=> t("Choisir un login différent de root ou de admin"),
          "importance" => WARNING,
      ),
      "spam" => array(
          "description" => t("Nombreuse tentatives d'inscription"),
          "help"=> t("Mettre en place un système de captcha et/ou désactiver le formulaire d'enregistrement de nouvel utilisateur"),
          "importance" => WARNING,
      ),
  );
}

/**
 * Test de la version de Drupal installée / présence d'une mise à jour
 * @return boolean true si obsolète false sinon
 */
function lba_insight_security_core_status() {
  global $base_url;
  
  module_load_include("inc", "update","update.fetch");
  
  //version installée
  $version_major = substr(VERSION, 0, 1);
  $version_patch = substr(VERSION, 2);
  
  //version disponible
  $url = "http://updates.drupal.org/release-history/drupal/".$version_major.".x";
  $xml = drupal_http_request($url);
  if (!isset($xml->error) && isset($xml->data)) {
    $data = $xml->data;
    if (!empty($data)) {
      $available = update_parse_xml($data);
      foreach($available["releases"] as $release) {
        if($release["status"]=="published" &&  $release["version_major"]==$version_major) {
          $version_patch_find = $release["version_patch"];
          variable_set("lba_insigth_performance_version_available",$release);
          return !($version_patch_find>$version_patch);
        }
      }
    }
  }
  variable_set("lba_insigth_performance_version_available",NULL);
  return true;
}


/**
 * Test de la présence de mise à jour de sécurité pour les modules
 * @return boolean true si obsolètes false sinon
 */
function lba_insight_security_modules_status() {
  global $base_url;

  $modules_to_update = array();
  
  module_load_include("inc", "update","update.fetch");
  
  $version_major_drupal = substr(VERSION, 0, 1);
  
  //Liste des modules
  $modules = array();
  $result = db_query("SELECT filename, name, type, status, schema_version, info FROM {system} WHERE type = 'module'");
  foreach ($result as $file) {
    $file->info = unserialize($file->info);
    $modules[$file->filename] = $file;
  }
  //var_dump($modules);
  foreach ($modules as $filename => $file) {
    $module_path = explode('/', $file->filename);
    array_pop($module_path);
    
    // We really only care about this module if it is in 'sites' folder.
    if ($module_path[0]=='sites' && $file->name && $file->info["version"] && $file->status) {
      
      $name = $file->name;
      
      $tmp = explode("-",$file->info["version"]);
      $tmp = explode(".",isset($tmp[1])?$tmp[1]:$tmp[0]);
      
      //version installée
      $version_major = $tmp[0];
      $version_patch = $tmp[1];
      
      $url = "http://updates.drupal.org/release-history/".$name."/".$version_major_drupal.".x";
      $xml = drupal_http_request($url);
      if (!isset($xml->error) && isset($xml->data)) {
        $data = $xml->data;
        if (!empty($data)) {
          $available = update_parse_xml($data);
          if($available["releases"]) {
            foreach($available["releases"] as $release) {
              
              //Security update
              $sec_update=false;
              if(isset($release["terms"]["Release type"])) {
                foreach($release["terms"]["Release type"] as $term) {
                  if($term=="Security update") {
                    $sec_update = true;
                    break;
                  }
                }
              }
              
              if($sec_update && $release["status"]=="published" && $release["version_major"]==$version_major && isset($release["version_patch"]) && (!isset($release["version_extra"]) || substr($release["version_extra"],0,2)!="rc")) {
                $version_patch_find = $release["version_patch"];
                $release["old_version"] = $name." ".$file->info["version"];
                if($version_patch_find>$version_patch)
                  $modules_to_update[$name]=$release;
                break;
              }
            }
          }
        }
      }
      
    }
  }
  
  variable_set("lba_insigth_performance_modules_to_update",$modules_to_update);
  
  return !count($modules_to_update);
}

/**
 * Test si la site est en ligne (non en maintenance)
 * @return boolean true si actif false sinon
 */
function lba_insight_security_maintenance() {
  return !variable_get("maintenance_mode",false);
}

/**
 * Test si il reste des fichiers TXT/PHP à supprimer à la racine
 * @return boolean true si actif false sinon
 */
function lba_insight_security_check_files_present() {
  $files_exist = FALSE;
  $url = url(NULL, array('absolute' => TRUE));
  $files_to_remove = array('CHANGELOG.txt', 'COPYRIGHT.txt', 'INSTALL.mysql.txt', 'INSTALL.pgsql.txt', 'INSTALL.txt', 'LICENSE.txt',
      'MAINTAINERS.txt', 'README.txt', 'UPGRADE.txt', 'PRESSFLOW.txt', 'install.php');
  
  foreach ($files_to_remove as $file) {
    $path = $_SERVER['DOCUMENT_ROOT'] . base_path() . $file;
    if (file_exists($path))
      $files_exist = TRUE;
  }
  
  return !$files_exist;
}

/**
 * Test si settings.php is read-only
 * @return boolean true si read-only false sinon
 */
function lba_insight_security_settings_permissions() {
  $settings_permissions_read_only = TRUE;
  $writes = array('2', '3', '6', '7'); // http://en.wikipedia.org/wiki/File_system_permissions
  $settings_file = './' . conf_path(FALSE, TRUE) . '/settings.php';
  $permissions = drupal_substr(sprintf('%o', fileperms($settings_file)), -4);
  foreach ($writes as $bit) {
    if (strpos($permissions, $bit)) {
      $settings_permissions_read_only = FALSE;
      break;
    }
  }
  return $settings_permissions_read_only;
}

/**
 * Determine if the super user has a weak name
 * @return boolean true si weak name false sinon
 */
function lba_insight_security_super_name() {
  $result = db_query("SELECT name FROM {users} WHERE uid = 1 AND (name LIKE '%admin%' OR name LIKE '%root%')")->fetch();
  if ($result) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Determine if the anonymous user exist
 * @return boolean true si exist false sinon
 */
function lba_insight_security_anonyme_exist() {
  $result = db_query("SELECT name FROM {users} WHERE uid = 0")->fetch();
  if ($result) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Determine if the admin user exist
 * @return boolean true si exist false sinon
 */
function lba_insight_security_admin_exist() {
  $result = db_query("SELECT name FROM {users} WHERE uid = 1")->fetch();
  if ($result) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Determine if the update script is accessible by everyone
 * @return boolean true si accessible false sinon
 */
function lba_insight_security_access_update() {
  global $update_free_access;
  return !$update_free_access;
}

/**
 * Test si beaucoup d'utilisateur bloqué -> SPAM
 */
function lba_insight_security_spam() {
  $result = db_query("SELECT COUNT(uid) FROM {users} WHERE status = 0")->fetchField(0);
  return $result<100;
}