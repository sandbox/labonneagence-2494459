<?php

/**
 * Affichage des informations sur la version de Drupal, les mises à jour de sécurité ...
 */
function lba_insight_performance_overview() {
  
  
  if(isset($_GET["site"])) {
  
    module_load_include("inc", "lba_insight_master","sites");
  
    $sid = $_GET["site"];
    $site = db_select("insight_site","s")
    ->condition("s.sid",$sid)
    ->fields("s",array("sid","sitename","url","token","score"))
    ->execute()->fetchAssoc();
  
    drupal_set_title(t("LBA Insight - Performance for :sitename",array(":sitename"=>$site["sitename"])),PASS_THROUGH);
    
    $data = array();
    
    $infos = lba_insight_master_get_info_site($sid);
    
    //scores
    $data["score"] =  $site["score"]?$site["score"]:0;
    $data["score_performance"] = $infos["performance"]["score"]?$infos["performance"]["score"]:0;
    
    //Récupération des tests à effectuer
    $data["tests"] = $infos["performance"]["tests"];
    
    //Exécution des tests
    $ret = $infos["performance"]["state"];
    
  } else {
  
    drupal_set_title("LBA Insight - Performance");
    $data = array();
    
    //scores
    $data["score"] = lba_insight_get_score();
    $data["score_performance"] = lba_insight_get_score("performance");
    
    //Récupération des tests à effectuer
    $data["tests"] = lba_insight_performance_tests();
    
    //Exécution des tests
    $ret = lba_insight_make_tests("performance",$data["tests"]);
    
  }
  
  return theme("performance_overview",$data);
}

/**
 * Liste des tests effectués par le module
 */
function lba_insight_performance_tests() {
  return array(
      "fast404" => array(
         "description" => t("Mode fast404 activé dans le fichier settings.php"),
         "help"=> t("Activer le mode Fast404 en décommentant la ligne # drupal_fast_404();"),
         "importance" => ERROR,
       ),
      "css_compression" => array(
         "description" => t("Aggrégation et compression des fichiers CSS"),
         "help"=> t("Activer l'aggrégation et la compression des fichiers CSS dans les performances [URL]/admin/config/development/performance"),
         "importance" => ERROR,
       ),
      "js_compression" => array(
         "description" => t("Aggrégation et compression des fichiers JS"),
         "help"=> t("Activer l'aggrégation et la compression des fichiers JS dans les performances [URL]/admin/config/development/performance"),
         "importance" => ERROR,
       ),
      "page_compression" => array(
         "description" => t("Activer la compression des pages"),
         "help"=> t("Activer la compression des pages dans les performances [URL]/admin/config/development/performance"),
         "importance" => ERROR,
       ),
      "cache" => array(
         "description" => t("Activer le cache"),
         "help"=> t("Activer le cache dans les performances [URL]/admin/config/development/performance"),
         "importance" => ERROR,
       ),
      "memcache" => array(
         "description" => t("Module memcache activé"),
         "help"=> t("Activer le module memcache et le paramétré dans le fichier settings.php"),
         "importance" => WARNING,
       ),
      "apc" => array(
         "description" => t("Extension APC de PHP activée"),
         "help"=> t("Activer l'extension APC de PHP"),
         "importance" => WARNING,
       ),
  );
}

/**
 * Test du mode fast404
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_fast404() {
  global $base_url;
  
  $path = sprintf('lba-insight-fast-404-check-%s.css', uniqid());
  $url  = rtrim($base_url, DIRECTORY_SEPARATOR) . base_path() . $path;
  
  $response = drupal_http_request($url);
  $expected = variable_get('404_fast_html', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>');
  $expected = strtr($expected, array('@path' => base_path() . $path));
  
  if (isset($response->code) && $response->code != 404) {
    return FALSE;
  }
  return (isset($response->data) && $response->data == $expected);
}

/**
 * Test si la compression CSS est active
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_css_compression() {
  return variable_get("preprocess_css",false);
}

/**
 * Test si la compression JS est active
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_js_compression() {
  return variable_get("preprocess_js",false);
}

/**
 * Test si la compression de page est active
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_page_compression() {
  return variable_get("page_compression",false);
}

/**
 * Test si le cache est actif
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_cache() {
  return variable_get("cache",false) && variable_get("cache_lifetime",false);
}

/**
 * Test si le module memcache est activé
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_memcache() {
  return module_exists('memcache');
}

/**
 * Test si l'extension PHP APC est activé
 * @return boolean true si actif false sinon
 */
function lba_insight_performance_apc() {
  return extension_loaded('apc');
}